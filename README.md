# This project allows to automate deployment of a django project with Ansible tool

# Deploy django project

We will be able to create dynamically services as:
        - ** Nginx **
        - ** supervisor **
        - ** postgres **

## 1- Setup server

-We can add a user in server with ssh authorized key.
-Disable Root login permission and password authetication

skip this step if user already created on server
```bash
ansible-playblook -i inventory -k setup_server.yml -u root -b -K
```
## 2- Server packages installations

Install all server dependencies needed

```bash
ansible-playbook -i inventory -k server_packages.yml -u {{user}} -b -K
```

## 3- Services configurations

```bash
ansible-playbook -i inventory -k playbook.yml -u {{user}} -b -K
```

Enable firewall

```bash
ansible-playbook -i inventory -k firewall.yml -u {{user}} -b -K
```

